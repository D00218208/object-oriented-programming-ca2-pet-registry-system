package PatrickNugentGD2OOPCA2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;

/**
 * Student ID: D00218208
 * @author Patrick Nugent
 */
public class Main 
{
    private static Scanner keyboard = new Scanner(System.in);
    private static ArrayList<Mammal> mammals = new ArrayList<Mammal>();
    private static ArrayList<Bird> birds = new ArrayList<Bird>();
    private static ArrayList<Fish> fishes = new ArrayList<Fish>();
    public static ArrayList<Owner> owners = new ArrayList<Owner>();
    public static ArrayList<Pet> pets = new ArrayList <Pet>();
    

    public enum MenuEnum 
    {
        SHUTDOWN(0), ADDPET(1), PETDETAILS(2), ADDOWNER(3), OWNERDETAILS(4), ASSIGNPET(5), PRINTPETS(6), PETSTATS(7), PRINTMENU(8);

        private int choice;
        private static Map map = new HashMap<>();

        private MenuEnum(int choice)
        {
            this.choice = choice;
        }

        static 
        {           
            for(MenuEnum menuEnum : MenuEnum.values()) 
            {
                map.put(menuEnum.choice, menuEnum);               
            }
        }
        
        /**
         *Takes in an integer and returns the corresponding enumeration so that
         * it can be used in the switch statement.
         * @param menuEnum
         * @return (MenuEnum)map.get(menuEnum)
         */
        public static MenuEnum valueOf(int menuEnum)
        {
            return (MenuEnum)map.get(menuEnum);
        }
    }
    
    /**
     *Main method where the main menu system resides. The menu system calls
     * other methods once a selection is made.
     * @param args
     */
    public static void main(String[] args) 
    { 
        Pets.readPetDataFromTextFile();
        Owners.readOwnerDataFromTextFile();
        printChoices();
        boolean quit = false;
        
        while(quit == false)
        {
            try{
            System.out.println("\nEnter choice: (Enter 8 for list of available choices)");
            int choice = keyboard.nextInt();
            
            if(choice < 0 || choice > 8)
            {
                System.out.println("Please enter one of the listed numbers (0-8)");
                continue;
            }
            
            MenuEnum menuEnum = MenuEnum.valueOf(choice);
            keyboard.nextLine();  
     
                switch(menuEnum) 
                {
                    case SHUTDOWN: 
                        System.out.println("Shutting down...");
                        Pets.writePetDataToTextFile();
                        Owners.writeOwnerDataToTextFile();
                        quit = true;
                        break;
                    case ADDPET: 
                        chooseGroup();
                        break;
                    case PETDETAILS:
                        petDetailsMenu();
                        break;
                    case ADDOWNER: 
                        addOwner();
                        break;
                    case OWNERDETAILS: 
                        ownerDetailsMenu();
                        break;
                    case ASSIGNPET:
                        assignPet();
                        break;
                    case PRINTPETS: 
                        printPetsMenu();
                        break;
                    case PETSTATS: 
                        generateStatistics();
                        break;
                    case PRINTMENU:
                        printChoices();
                        break;
                    default:
                        System.out.println("Please enter one of the listed numbers.");
                        break;
                }
            }
            catch(InputMismatchException e)
            {
                System.out.println("Please enter one of the listed numbers (0-8)");
                keyboard.nextLine();  
            }
        }      
    }
    
    /**
     *Menu system that allows the user to choose a pet type for a new pet. 
     * The addPet method is called once a selection is made
     * other methods once a selection is made.
     */
    private static void chooseGroup()
    {
        boolean finished = false;
        
        while(finished == false)
        {
            try{        
            System.out.println("\nEnter the animal group of the pet: \npress");
            System.out.println("1 - to add a new mammal\n" +
                               "2 - to add a new bird\n" +
                               "3 - to add a new fish\n");
        
            int group = keyboard.nextInt();
                             
            if(group < 1 || group > 3)
            {
                System.out.println("Please enter one of the listed numbers (0-3)");
                continue; 
            }
                switch(group) 
                {
                    case 1: 
                        addPet("mammal");
                        finished = true;
                        break;
                    case 2: 
                        addPet("bird");
                        finished = true;
                        break;
                    case 3:
                        addPet("fish");
                        finished = true;
                        break;
                }
            }
            catch(InputMismatchException e)
            {
                System.out.println("Please enter one of the listed numbers (0-3)");
                keyboard.nextLine();  
            }
        } 
    }
    
    /**
     *Prompts the user to enter the attributes of the pet and creates 
     * a new pet using these attributes. 
     * @param group pet group (mammal, bird, fish)
     */
    private static void addPet(String group)
    {
        keyboard.nextLine();  
        System.out.println("\nEnter the type of pet (e.g. Dog, Cat, Budgie, Goldfish, etc.)");
        String type = keyboard.nextLine();
        
        System.out.println("\nEnter the name of the pet");
        String name = keyboard.nextLine();
        
        System.out.println("\nEnter the breed of the pet");
        String breed = keyboard.nextLine();
        
        int age = chooseAge();
        
        ArrayList<String> petColours = chooseColours();
        
        keyboard.nextLine();  
        System.out.println("\nEnter the gender of the pet");
        String gender = keyboard.nextLine();
        
        boolean neutered = false;
        double wingspan = 0;
        boolean canFly = false;
        String waterType = "fresh";
        
        if(group.equals("mammal"))
        {
            neutered = isNeutered();           
        }
        else if(group.equals("bird"))
        {           
            wingspan = inputWingspan();                       
            canFly = canFly();
        }
        else
        {
            System.out.println("\nEnter the required water type of the fish");
            waterType = keyboard.nextLine();
        }
             
        String day = chooseDay();
                
        String month = chooseMonth();

        String year = chooseYear();      
        
        String dateRegistered = day.concat("-").concat(month).concat("-").concat(year);
       
        String petID = null;       
        String ownerID = null;
        
        if(group.equals("mammal"))
        {
            int idNumber = pets.size();
            petID = "PID".concat(Integer.toString(idNumber));
            Mammal newMammal = new Mammal(type, name, breed, age, petColours, gender, neutered, dateRegistered, petID, ownerID); 
            boolean isDuplicate = findDuplicates(group, newMammal, null, null);
            if(isDuplicate == false)
            {            
                String newPetID = checkPetID(petID, idNumber);
                newMammal.setPetID(newPetID);
                System.out.println("\nPet ID assigned: " + petID); 
                pets.add(newMammal);
                mammals.add(newMammal);
                System.out.println("Pet added successfully");    
            }
            else
            {
                System.out.println("The pet is a duplicate and cannot be added");
            }
        }
        else if(group.equals("bird"))
        {        
            int idNumber = pets.size();
            petID = "PID".concat(Integer.toString(idNumber));
            Bird newBird = new Bird(type, name, breed, age, petColours, gender, wingspan, canFly, dateRegistered, petID, ownerID); 
            boolean isDuplicate = findDuplicates(group, null, newBird, null);
            if(isDuplicate == false)
            {            
                String newPetID = checkPetID(petID, idNumber);
                newBird.setPetID(newPetID);
                System.out.println("\nPet ID assigned: " + petID); 
                pets.add(newBird);
                birds.add(newBird);
                System.out.println("Pet added successfully");    
            }
            else
            {
                System.out.println("The pet is a duplicate and cannot be added");
            }
        }
        else
        {
            int idNumber = pets.size();
            petID = "PID".concat(Integer.toString(idNumber));
            Fish newFish = new Fish(type, name, breed, age, petColours, gender, waterType, dateRegistered, petID, ownerID);
            boolean isDuplicate = findDuplicates(group, null, null, newFish);
            if(isDuplicate == false)
            {            
                String newPetID = checkPetID(petID, idNumber);
                newFish.setPetID(newPetID);
                System.out.println("\nPet ID assigned: " + petID); 
                pets.add(newFish);
                fishes.add(newFish);
                System.out.println("Pet added successfully");    
            }
            else
            {
                System.out.println("The pet is a duplicate and cannot be added");
            }
        }
    }
    
    /**
     *Prompts the user to enter the attributes of the pet and creates 
     * a new pet using these attributes. 
     * @param group animal group
     * @param newMammal an existing mammal (may be null)
     * @param newBird an existing bird (may be null)
     * @param newFish an existing fish (may be null)
     */
    private static boolean findDuplicates(String group, Mammal newMammal, Bird newBird, Fish newFish)
    {
        if(group.equals("mammal"))
        {
            for(Mammal mammal : mammals) 
            {
                if(mammal.equals(newMammal) == false)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
        else if(group.equals("bird"))
        {
            for(Bird bird : birds) 
            {
                if(bird.equals(newBird) == false)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            } 
        }
        else if(group.equals("fish"))
        {
            for(Fish fish : fishes) 
            {
                if(fish.equals(newFish) == false)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }   
        }    
        return false;
    }
    
    /**
     *Prompts the user to enter the attributes of the pet and creates 
     * a new pet using these attributes. 
     * @param petID an ID of a pet
     * @param idNUmber the number at the end of an ID
     * @return petID the new petID (may remain the same)
     */
    private static String checkPetID(String petID, int idNumber)
    {
        for (Mammal mammal : mammals) 
        {
            if(mammal.getPetID().equals(petID))
            {
                idNumber = idNumber + 1;
                petID = "PID".concat(Integer.toString(idNumber));
                return petID;
            }
        }
        
        for (Bird bird : birds) 
        {
            if(bird.getPetID().equals(petID))
            {
                idNumber = idNumber + 1;
                petID = "PID".concat(Integer.toString(idNumber));
                return petID;
            }
        }
        
        for (Fish fish : fishes) 
        {
            if(fish.getPetID().equals(petID))
            {
                idNumber = idNumber + 1;
                petID = "PID".concat(Integer.toString(idNumber));
                return petID;
            }
        }
        
        return petID;       
    }
    
    /**
     *Prompts the user to enter the age of the new pet
     * @return age
     */
    private static int chooseAge()
    {
        boolean finished = false;
        int age = 0;
        
        while(finished == false)
        {
            System.out.println("\nEnter the age of the pet");
            try{
                age = keyboard.nextInt();
            }
            catch(InputMismatchException e)
            {
                System.out.println("\nPlease enter an integer for the age");  
                keyboard.nextLine();
                continue;
            }
            finished = true;
        }
        return age;
    }
    
    /**
     *Prompts the user to enter the colours of the new pet
     * @return colours
     */
    private static ArrayList<String> chooseColours()
    {
        ArrayList<String> petColours = new ArrayList<String>();
        boolean finished = false;
        
        keyboard.nextLine();
        System.out.println("\nEnter the colour of the pet"); 
        String colour = keyboard.nextLine();
        
        petColours.add(colour);
        
        while(finished == false)
        {            
            System.out.println("\nEnter 1 to add another colour or 0 to finish adding colours");
            
            try{
                int selection = keyboard.nextInt();
                if(selection == 1)
                {
                    keyboard.nextLine();
                    System.out.println("\nEnter the colour of the pet");
                    String newColour = keyboard.nextLine();
                    petColours.add(newColour);                   
                }
                else if(selection == 0)
                {
                    finished = true;
                }
            }
            catch(InputMismatchException e)
            {
                System.out.println("\nPlease enter 1 to add another colour or 0 to finish adding colours"); 
                keyboard.nextLine();
                continue;
            }
        }
        
        return petColours;       
    }
    
    /**
     *Prompts the user to enter the day of registry
     * @return day
     */
    private static String chooseDay()
    {
        boolean finished = false;
        int day = 1;
        
        while(finished == false)
        {
            try{     
                System.out.println("\nEnter the day that the pet was registered (1-31)");
                day = keyboard.nextInt();
                
                if(day < 1 || day > 31)
                {
                    System.out.println("\nPlease enter a day between 1 and 31");           
                    continue;
                }
            }
            catch(InputMismatchException e)
            {
                System.out.println("\nPlease enter an integer between 1 and 31"); 
                keyboard.nextLine();
                continue;                
            }
            
            finished = true;
        }     
        
        return Integer.toString(day);
    }
    
    
    /**
     *Prompts the user to enter the month of registry
     * @return month
     */
    private static String chooseMonth()
    {
        boolean finished = false;
        int month = 1;
                
        while(finished == false)
        {
            try{     
                System.out.println("\nEnter the month that the pet was registered (1-12)");
                month = keyboard.nextInt();
                
                if(month < 1 || month > 12)
                {
                    System.out.println("\nPlease enter a month between 1 and 12");
                    continue;
                }
            }
            catch(InputMismatchException e)
            {
                System.out.println("\nPlease enter an integer between 1 and 12");
                keyboard.nextLine();
                continue;
            }
            
            finished = true;
        }     
        
        return Integer.toString(month);
    }
    
    /**
     *Prompts the user to enter the year of registry
     * @return year
     */
    private static String chooseYear()
    {
        boolean finished = false;        
        int year = 0;
        
        while(finished == false)
        {
            try{      
                System.out.println("\nEnter the year that the pet was registered (0-99)");
                year = keyboard.nextInt();
                
                if(year < 0 || year > 99)
                {
                    System.out.println("\nPlease enter a year between 0 and 99");  
                    continue;
                }
            }
            catch(InputMismatchException e)
            {
                System.out.println("\nPlease enter an integer between 0 and 99");      
                keyboard.nextLine();
                continue;
            }
            
            finished = true;
        }     
        
        return Integer.toString(year);
    } 
    
    /**
     *Prompts the user to enter whether mammal is neutered
     * @return isNeutered
     */
    private static boolean isNeutered()
    {
        boolean finished = false; 
        boolean isNeutered = false;
        
        while(finished == false)
        {
            try{      
                System.out.println("\nEnter\npress");
                System.out.println("1 - if the pet is neutered\n" +
                                   "2 - if the pet is not neutered\n");
                int choice = keyboard.nextInt();
                
                if(choice == 1)
                {
                    isNeutered = true;
                }
                else if(choice == 2)
                {
                    isNeutered = false;
                }
                else
                {
                    System.out.println("\nPlease enter either 1 or 2");  
                    continue;
                }
            }
            catch(InputMismatchException e)
            {
                System.out.println("\nPlease enter either 1 or 2");      
                keyboard.nextLine();
                continue;
            }           
            finished = true;
        }            
        return isNeutered;
    }
    
    /**
     *Prompts the user to enter whether bird can fly
     * @return canFly
     */
    private static boolean canFly()
    {
        boolean finished = false; 
        boolean canFly = false;
        
        while(finished == false)
        {
            try{      
                System.out.println("\nEnter\npress");
                System.out.println("1 - if the bird can fly\n" +
                                   "2 - if the bird cannot fly\n");
                int choice = keyboard.nextInt();
                
                if(choice == 1)
                {
                    canFly = true;
                }
                else if(choice == 2)
                {
                    canFly = false;
                }
                else
                {
                    System.out.println("\nPlease enter either 1 or 2");  
                    continue;                  
                }
            }
            catch(InputMismatchException e)
            {
                System.out.println("\nPlease enter either 1 or 2");      
                keyboard.nextLine();
                continue;
            }          
            finished = true;
        }            
        return canFly;   
    }
    
    /**
     *Prompts the user to enter the wingspan of the bird
     * @return wingspan
     */
    private static double inputWingspan()
    {
        boolean finished = false; 
        double wingspan = 0;
        
        while(finished == false)
        {
            try{      
                System.out.println("\nEnter the wingspan of the bird in inches");
                wingspan = keyboard.nextDouble();
            }
            catch(InputMismatchException e)
            {
                System.out.println("Please enter a number for the wingspan");
                keyboard.nextLine();
                continue;
            }           
            finished = true;
        }            
        return wingspan;      
    }
    
    /**
     *Menu system that allows the user to edit/delete/print pet details
     */
    private static void petDetailsMenu()
    {
        System.out.println("\nAvailable choices: \npress");
        System.out.println("1 - to edit pet details\n" +
                           "2 - to delete pet details\n" +
                           "3 - to print pet details"); 
        
        boolean quit = false;
        
        while(quit == false)
        {
            try{
            System.out.println("\nEnter choice:");
            int choice = keyboard.nextInt();
            
            if(choice < 1 || choice > 3)
            {
                System.out.println("Please enter one of the listed numbers (1-3)");
                continue;
            }    
                switch(choice) 
                {
                    case 1: 
                        editPetDetails();
                        quit = true;
                        break;
                    case 2: 
                        deletePetDetails();
                        quit = true;
                        break;
                    case 3:
                        printPetDetails();
                        quit = true;
                        break;
                }
            }
            catch(InputMismatchException e)
            {
                System.out.println("Please enter one of the listed numbers (1-3)");
                keyboard.nextLine();                            
            }
        }      
    }
    
    /**
     *Returns a pet matching ID
     * @param petID
     * @return Pet
     */
    private static Pet getPet(String petID)
    {
        for(int i=0; i<pets.size(); ++i)
        {
            if(petID.equals(pets.get(i).getPetID()))
            {
                return pets.get(i);
            }
            else
            {
                return null;
            }
        }
        return null;
    }
    
    /**
     *Returns a mammal matching ID
     * @param petID
     * @return Mammal
     */
    private static Mammal getMammal(String petID)
    {
        for(int i=0; i<mammals.size(); ++i)
        {
            if(petID.equals(mammals.get(i).getPetID()))
            {
                return mammals.get(i);
            }
            else
            {
                return null;
            }
        }
        return null;
    }
    
    /**
     *Returns a bird matching ID
     * @param petID
     * @return Bird
     */
    private static Bird getBird(String petID)
    {
        for(int i=0; i<birds.size(); ++i)
        {
            if(petID.equals(birds.get(i).getPetID()))
            {
                return birds.get(i);
            }
            else
            {
                return null;
            }
        }
        return null;
    }
    
    /**
     *Returns a fish matching ID
     * @param petID
     * @return Fish
     */
    private static Fish getFish(String petID)
    {
        for(int i=0; i<fishes.size(); ++i)
        {
            if(petID.equals(fishes.get(i).getPetID()))
            {
                return fishes.get(i);
            }
            else
            {
                return null;
            }
        }
        return null;
    }
    
    /**
     *Menu that allows user to edit pet details
     */
    private static void editPetDetails()
    {
        String group;
        System.out.println("\nEnter the ID of the pet that you want to edit");
        System.out.print("\nPID");
        
        String ID = "default";
        
        try{
        int IDNumber = keyboard.nextInt();
        ID = "PID".concat(Integer.toString(IDNumber));
        }
        catch(InputMismatchException e)
        {
            System.out.println("Please enter a number for the ID");
        }
        
        Pet existingPet = getPet(ID);
        Mammal existingMammal = getMammal(ID);
        Bird existingBird = getBird(ID);    
        Fish existingFish = getFish(ID);

        if(existingMammal == null & existingBird == null & existingFish == null)
        {
            System.out.println("No pet with that ID number was found");
            group = null;
        }
        
        if(existingMammal != null)
        {
            group = "mammal";
        }
        else if(existingBird != null)
        {
            group = "bird";
        }
        else if(existingFish != null)
        {
            group = "fish";
        }
        else
        {
            group = null;
        }
        
        if(group != null)
        {
            boolean quit = false;
        
            while(quit == false)
            {
                try{
                System.out.println("\npress");
                System.out.println("0 - to edit pet type\n" +
                               "1 - to edit pet name\n" +
                               "2 - to edit pet breed\n" +
                               "3 - to edit pet age\n" +
                               "4 - to edit pet colour\n" +
                               "5 - to edit pet gender\n" + 
                               "6 - to edit pet registry date\n" + 
                               "7 - to edit if the pet is neutered\n" +
                               "8 - to edit if the pet can fly\n" + 
                               "9 - to edit pet wingspan\n" + 
                               "10 - to edit pet water type\n"+
                               "11 - to finish editing pet details");   
                
                System.out.println("\nEnter choice:");
                int choice = keyboard.nextInt();
            
                if(choice < 0 || choice > 11)
                {
                    System.out.println("Please enter one of the listed numbers (0-11)");
                    continue;
                }    
                    switch(choice) 
                    {
                        case 0: 
                            editType(group, existingMammal, existingBird, existingFish, existingPet);                       
                            break;
                        case 1: 
                            editName(group, existingMammal, existingBird, existingFish, existingPet);     
                            break;
                        case 2:
                            editBreed(group, existingMammal, existingBird, existingFish, existingPet);   
                            break;
                        case 3:
                            editAge(group, existingMammal, existingBird, existingFish, existingPet);
                            break;
                        case 4:
                            editColours(group, existingMammal, existingBird, existingFish, existingPet);
                            break;
                        case 5:
                            editGender(group, existingMammal, existingBird, existingFish, existingPet);   
                            break;
                        case 6:
                            editDateRegistered(group, existingMammal, existingBird, existingFish, existingPet);                             
                            break;
                        case 7:
                            editNeutered(group, existingMammal, existingPet);
                            break;
                        case 8:
                            editFly(group, existingBird, existingPet);
                            break;
                        case 9:
                            editWingspan(group, existingBird, existingPet);
                            break;
                        case 10:
                            editWaterType(group, existingFish, existingPet);
                            break;
                        case 11:
                            quit = true;
                            break;
                    }
                }
                catch(InputMismatchException e)
                {
                    System.out.println("Please enter one of the listed numbers (0-3)");
                    keyboard.nextLine();                            
                }
            }
        }
        else
        {
            System.out.println("No pet with that ID number was found");
        }
    }
    
    /**
     *Menu that allows user to edit pet details
     */
    private static void editType(String group, Mammal existingMammal, Bird existingBird, Fish existingFish, Pet existingPet)
    {     
        keyboard.nextLine();
        System.out.println("\nEnter the type of pet");
        String type = keyboard.nextLine();    
        if(group.equals("mammal"))
        {
            existingMammal.setType(type);
            existingPet.setType(type);
        }
        else if(group.equals("bird"))
        {
            existingBird.setType(type);
            existingPet.setType(type);
        }
        else if(group.equals("fish"))
        {
            existingFish.setType(type);
            existingPet.setType(type);
        }
        System.out.println("Type edited successfully");    
    }
    
    /**
     *Allows user to edit name of pet
     * @param group
     * @param existingMammal
     * @param existingBird
     * @param existingFish
     * @param existingPet
     */
    private static void editName(String group, Mammal existingMammal, Bird existingBird, Fish existingFish, Pet existingPet)
    {
        keyboard.nextLine();     
        System.out.println("Enter the name of the pet");
        String name = keyboard.nextLine();
        if(group.equals("mammal"))
        {
            existingMammal.setName(name);
            existingPet.setName(name);
        }
        else if(group.equals("bird"))
        {
            existingBird.setName(name);
            existingPet.setName(name);
        }
        else if(group.equals("fish"))
        {
            existingFish.setName(name);
            existingPet.setName(name);
        }
        System.out.println("Name edited successfully");        
    }
    
    /**
     *Allows user to edit breed of pet
     * @param group
     * @param existingMammal
     * @param existingBird
     * @param existingFish
     * @param existingPet
     */
    private static void editBreed(String group, Mammal existingMammal, Bird existingBird, Fish existingFish, Pet existingPet)
    {
        keyboard.nextLine();     
        System.out.println("Enter the breed of the pet");
        String breed = keyboard.nextLine();
        if(group.equals("mammal"))
        {
            existingMammal.setBreed(breed);
            existingPet.setBreed(breed);
        }
        else if(group.equals("bird"))
        {
            existingBird.setBreed(breed);
            existingPet.setBreed(breed);
        }
        else if(group.equals("fish"))
        {
            existingFish.setBreed(breed);
            existingPet.setBreed(breed);
        }
        System.out.println("Breed edited successfully");           
    }
    
    /**
     *Allows user to edit age of pet
     * @param group
     * @param existingMammal
     * @param existingBird
     * @param existingFish
     * @param existingPet
     */
    private static void editAge(String group, Mammal existingMammal, Bird existingBird, Fish existingFish, Pet existingPet)
    {
        int newAge = chooseAge();
        if(group.equals("mammal"))
        {
            existingMammal.setAge(newAge);
            existingPet.setAge(newAge);
        }
        else if(group.equals("bird"))
        {
            existingBird.setAge(newAge);
            existingPet.setAge(newAge);
        }
        else if(group.equals("fish"))
        {
            existingFish.setAge(newAge);
            existingPet.setAge(newAge);
        }
        System.out.println("Age edited successfully");     
    }
    
    /**
     *Allows user to edit colours of pet
     * @param group
     * @param existingMammal
     * @param existingBird
     * @param existingFish
     * @param existingPet
     */
    private static void editColours(String group, Mammal existingMammal, Bird existingBird, Fish existingFish, Pet existingPet)
    {
        ArrayList<String> newColours = chooseColours();
        if(group.equals("mammal"))
        {
            existingMammal.setColour(newColours);
            existingPet.setColour(newColours);
        }
        else if(group.equals("bird"))
        {
            existingBird.setColour(newColours);
            existingPet.setColour(newColours);
        }
        else if(group.equals("fish"))
        {
            existingFish.setColour(newColours);
            existingPet.setColour(newColours);
        }
        System.out.println("Colour edited successfully");      
    }
    
    /**
     *Allows user to edit gender of pet
     * @param group
     * @param existingMammal
     * @param existingBird
     * @param existingFish
     * @param existingPet
     */
    private static void editGender(String group, Mammal existingMammal, Bird existingBird, Fish existingFish, Pet existingPet)
    {
        keyboard.nextLine();     
        System.out.println("Enter the gender of the pet");
        String newGender = keyboard.nextLine();
        if(group.equals("mammal"))
        {
            existingMammal.setGender(newGender);
            existingPet.setGender(newGender);
        }
        else if(group.equals("bird"))
        {
            existingBird.setGender(newGender);
            existingPet.setGender(newGender);
        }
        else if(group.equals("fish"))
        {
            existingFish.setBreed(newGender);
            existingPet.setGender(newGender);
        }
        System.out.println("Gender edited successfully");       
    }
    
    /**
     *Allows user to edit registry date of pet
     * @param group
     * @param existingMammal
     * @param existingBird
     * @param existingFish
     * @param existingPet
     */
    private static void editDateRegistered(String group, Mammal existingMammal, Bird existingBird, Fish existingFish, Pet existingPet)
    {
        String newDay = chooseDay();
        String newMonth = chooseMonth();
        String newYear = chooseYear();
        
        String newDateRegistered = newDay.concat("-").concat(newMonth).concat("-").concat(newYear);
        
        if(group.equals("mammal"))
        {
            existingMammal.setDateRegistered(newDateRegistered);
            existingPet.setDateRegistered(newDateRegistered);
        }
        else if(group.equals("bird"))
        {
            existingBird.setDateRegistered(newDateRegistered);
            existingPet.setDateRegistered(newDateRegistered);
        }
        else if(group.equals("fish"))
        {
            existingFish.setDateRegistered(newDateRegistered);
            existingPet.setDateRegistered(newDateRegistered);
        }
        System.out.println("Registry date edited successfully");       
    }
    
    /**
     *Allows user to edit neutered status of mammal
     * @param group
     * @param existingMammal
     * @param existingPet
     */
    private static void editNeutered(String group, Mammal existingMammal, Pet existingPet)
    {
        if(group.equals("mammal"))
        {
            boolean neutered = isNeutered();
            pets.remove(existingMammal);
            existingMammal.setNeutered(neutered); 
            pets.add(existingMammal);
            
            System.out.println("Neutered status edited successfully");
        }
        else
        {
            System.out.println("The selected pet is not a mammal therefore this attribute cannot be edited");
        }             
    }
    
    /**
     *Allows user to edit flying capability of bird
     * @param group
     * @param existingBird
     * @param existingPet
     */
    private static void editFly(String group, Bird existingBird, Pet existingPet)
    {
        if(group.equals("bird"))
        {
            boolean canFly = canFly();
            pets.remove(existingBird);
            existingBird.setFly(canFly);
            pets.add(existingBird);
            System.out.println("Flying status edited successfully");    
        }
        else
        {
            System.out.println("The selected pet is not a bird therefore this attribute cannot be edited");
        }         
    }
    
    /**
     *Allows user to edit wingspan of bird
     * @param group
     * @param existingBird
     * @param existingPet
     */
    private static void editWingspan(String group, Bird existingBird, Pet existingPet)
    {
        if(group.equals("bird"))
        {
            double newWingspan = inputWingspan();
            pets.remove(existingBird);
            existingBird.setWingspan(newWingspan);
            pets.add(existingBird);
            System.out.println("Wingspan edited successfully");
        }
        else
        {
            System.out.println("The selected pet is not a bird therefore this attribute cannot be edited");
        }       
    }
    
    /**
     *Allows user to edit water type of fish
     * @param group
     * @param existingFish
     * @param existingPet
     */
    private static void editWaterType(String group, Fish existingFish, Pet existingPet)
    {
        keyboard.nextLine();     
        if(group.equals("fish"))
        {
            System.out.println("\nEnter the required water type of the fish");
            String newWaterType = keyboard.nextLine();
            pets.remove(existingFish);
            existingFish.setWaterType(newWaterType); 
            pets.add(existingFish);
            System.out.println("Water type edited successfully");
        }
        else
        {
            System.out.println("The selected pet is not a fish therefore this attribute cannot be edited");
        }       
    }
    
    /**
     *Deletes details of a pet found using an ID that the user enters
     */
    private static void deletePetDetails()
    {
        Pet existingPet = null;
        Mammal existingMammal = null;
        Bird existingBird = null;
        Fish existingFish = null;
        System.out.println("Enter the ID of the pet that you want to delete");
        System.out.print("\nPID");
        
        String ID = "default";
        
        try{
        int IDNumber = keyboard.nextInt();
        ID = "PID".concat(Integer.toString(IDNumber));
        }
        catch(InputMismatchException e)
        {
            System.out.println("Please enter a number for the ID");
        }
        
        existingPet = getPet(ID);
        existingMammal = getMammal(ID);
        existingBird = getBird(ID);
        existingFish = getFish(ID);        
        
        if(existingMammal == null && existingBird == null && existingFish == null)
        {
            System.out.println("No pet with that ID number was found");               
        }
        
        if(existingMammal != null)
        {
            mammals.remove(existingMammal);
            pets.remove(existingPet);
            System.out.println("Pet details deleted successfully");
        }
        else if(existingBird != null)
        {
            birds.remove(existingBird);
            pets.remove(existingPet);
            System.out.println("Pet details deleted successfully");
        }
        else if(existingFish != null)
        {
            fishes.remove(existingFish);
            pets.remove(existingPet);
            System.out.println("Pet details deleted successfully");
        }       
    }
    
    /**
     *Prints the details of every pet
     */
    private static void printPetDetails()
    {
        for (Pet pet : pets) 
        {
            System.out.println(pet.toString());
        }    
    }
    
    /**
     *Prompts the user to enter the attributes of the owner and creates 
     * a new owner using these attributes. 
     */
    private static void addOwner() 
    {
        System.out.println("\nEnter the name of the owner");
        String name = keyboard.nextLine();
        
        System.out.println("\nEnter the email of the owner");
        String email = keyboard. nextLine();
        
        System.out.println("\nEnter the telephone number of the owner");
        String telephone = keyboard.nextLine();
        
        System.out.println("\nEnter the address of the owner");
        String address = keyboard.nextLine();
        
        int idNumber = owners.size();
        String ID = "OID".concat(Integer.toString(idNumber));

        for (Owner owner : owners) 
        {
            if(owner.getID().equals(ID))
            {
                idNumber = idNumber + 1;
                ID = "OID".concat(Integer.toString(idNumber));
            }
        }  
        
        System.out.println("\nOwner ID assigned: " + ID); 
        Owner newOwner = new Owner(name, ID, email, telephone, address);  
        owners.add(newOwner);
        
        System.out.println("Owner added successfully");
    }
    
    /**
     *Menu system that allows the user to edit/delete/print owner details
     */
    private static void ownerDetailsMenu()
    {
        System.out.println("\nAvailable choices: \npress");
        System.out.println("1 - to edit owner details\n" +
                           "2 - to delete owner details\n" +
                           "3 - to print owner details"); 
        
        boolean quit = false;
        
        while(quit == false)
        {
            try{
            System.out.println("\nEnter choice:");
            int choice = keyboard.nextInt();
            
            if(choice < 1 || choice > 3)
            {
                System.out.println("Please enter one of the listed numbers (1-3)");
                continue;
            }    
                switch(choice) 
                {
                    case 1: 
                        editOwnerDetails();
                        quit = true;
                        break;
                    case 2: 
                        deleteOwnerDetails();
                        quit = true;
                        break;
                    case 3:
                        printOwnerDetails();
                        quit = true;
                        break;
                }
            }
            catch(InputMismatchException e)
            {
                System.out.println("Please enter one of the listed numbers (1-3)");
                keyboard.nextLine();                            
            }
        }      
    }
            
    /**
     *Returns an owner with matching ID
     * @param ownerID
     * @return Owner
     */
    private static Owner getOwner(String ownerID)
    {
        for(int i=0; i<owners.size(); ++i)
        {
            if(ownerID.equals(owners.get(i).getID()))
            {
                return owners.get(i);
            }
            else
            {
                return null;
            }
        }
        return null;
    }
    
    /**
     *Menu that allows user to edit pet details
     */
    private static void editOwnerDetails()
    {
        System.out.println("\nEnter the ID of the owner that you want to edit");
        System.out.print("\nOID");
        
        String ID = "default";
        
        try{
        int IDNumber = keyboard.nextInt();
        ID = "OID".concat(Integer.toString(IDNumber));
        }
        catch(InputMismatchException e)
        {
            System.out.println("Please enter a number for the ID");           
        }
        Owner existingOwner = getOwner(ID);

        if(existingOwner != null)
        {
            boolean quit = false;
        
            while(quit == false)
            {
                try{
                System.out.println("\npress");
                System.out.println("0 - to edit owner name\n" +
                               "1 - to edit owner email\n" +
                               "2 - to edit owner telephone number\n" +
                               "3 - to edit owner address\n" +
                               "4 - to finish editing owner details");   
                
                System.out.println("\nEnter choice:");
                int choice = keyboard.nextInt();
                keyboard.nextLine();
            
                if(choice < 0 || choice > 4)
                {
                    System.out.println("Please enter one of the listed numbers (0-4)");
                    keyboard.nextLine();
                    continue;
                }    
                    switch(choice) 
                    {
                        case 0: 
                            System.out.println("\nEnter the name of the owner");
                            String name = keyboard.nextLine(); 
                            existingOwner.setName(name);
                            break;
                        case 1: 
                            System.out.println("\nEnter the email of the owner");
                            String email = keyboard.nextLine(); 
                            existingOwner.setEmail(email);    
                            break;
                        case 2:
                            System.out.println("\nEnter the telephone number of the owner");
                            String telephone = keyboard.nextLine(); 
                            existingOwner.setTelephone(telephone);   
                            break;
                        case 3:
                            System.out.println("\nEnter the address of the owner");
                            String address = keyboard.nextLine(); 
                            existingOwner.setAddress(address);
                            break;
                        case 4:
                            quit = true;
                            break;
                    }
                }
                catch(InputMismatchException e)
                {
                    System.out.println("Please enter one of the listed numbers (0-3)");
                    keyboard.nextLine();                            
                }
            }
        }
        else
        {
            System.out.println("No owner with that ID number was found");
            keyboard.nextLine();
        }
    }
    
    /**
     *Deletes details of an owner found using an ID that the user enters
     */
    private static void deleteOwnerDetails()
    {
        Owner existingOwner = null;
        System.out.println("Enter the ID of the owner that you want to delete");
        System.out.print("\nOID");
        
        String ID = "default";
        
        try
        {
            int IDNumber = keyboard.nextInt();
            ID = "OID".concat(Integer.toString(IDNumber));
            keyboard.nextLine();
        }
        catch(InputMismatchException e)
        {
            System.out.println("Please enter a number for the ID");
        }
        
        for (Owner owner : owners) 
        {
            if(owner.getID().equals(ID))
            {
                existingOwner = owner;
            }
        }  
             
        if(existingOwner == null)
        {
            System.out.println("No owner with that ID number was found");               
        }
        
        if(existingOwner != null)
        {
            owners.remove(existingOwner);
            System.out.println("Owner details deleted successfully");
        }
             
    }
    
    /**
     *Prints the details of every owner
     */
    private static void printOwnerDetails()
    {
        for(Owner owner : owners) 
        {
            System.out.println(owner.toString());
        }     
    }
    
    /**
     *Assigns a pet to an owner once the ID of both are entered by the user
     */
    private static void assignPet()
    {
        System.out.println("\nEnter the ID of the pet that you want to assign");
        System.out.print("\nPID");
        
        String ID = "default";
        
        try{
        int IDNumber = keyboard.nextInt();
        ID = "PID".concat(Integer.toString(IDNumber));
        }
        catch(InputMismatchException e)
        {
            System.out.println("Please enter a number for the ID");
        }
        
        Pet existingPet = getPet(ID);
        Mammal existingMammal = getMammal(ID);
        Bird existingBird = getBird(ID);
        Fish existingFish = getFish(ID);
        
        if(existingMammal == null && existingBird == null && existingFish == null)
        {
            System.out.println("No pet with that ID number was found");
        }
        
        if(existingMammal != null)
        {
            Owner existingOwner = enterOwnerID();
            if(existingOwner != null)
            {
                existingMammal.setOwnerID(existingOwner.getID());
                existingPet.setOwnerID(existingOwner.getID());
                System.out.println("Pet assigned to owner successfully");
            }
        }
        else if(existingBird != null)
        {
            Owner existingOwner = enterOwnerID();
            if(existingOwner != null)
            {
                existingBird.setOwnerID(existingOwner.getID());
                existingPet.setOwnerID(existingOwner.getID());
                System.out.println("Pet assigned to owner successfully");
            }
        }
        else if(existingFish != null)
        {
            Owner existingOwner = enterOwnerID();
            if(existingOwner != null)
            {
                existingFish.setOwnerID(existingOwner.getID());
                existingPet.setOwnerID(existingOwner.getID());
                System.out.println("Pet assigned to owner successfully");
                keyboard.nextLine();
            }
        }
    }
    
    /**
     *Prompts user to enter the ID of the owner which is validated
     */
    private static Owner enterOwnerID()
    {
        System.out.println("\nEnter the ID of the owner that you want the pet to be assigned to");
        System.out.print("\nOID");
        
        String ID = "default";
        
        try{
        int IDNumber = keyboard.nextInt();
        ID = "OID".concat(Integer.toString(IDNumber));
        }
        catch(InputMismatchException e)
        {
            System.out.println("Please enter a number for the ID");
            keyboard.nextLine();
        }
        
        Owner existingOwner = getOwner(ID);
        if(existingOwner == null)
        {
            System.out.println("No Owner with that ID number was found");
        }
        return existingOwner;
    }
    
    /**
     *Menu system which calls a printing method once a selection is made.
     */
    private static void printPetsMenu()
    {
        boolean quit = false;
        
        while(quit == false)
            {
                try{
                System.out.println("\npress");
                System.out.println("0 - to print pets in order of ID\n" +
                               "1 - to print pets in order of gender\n" +
                               "2 - to print pets in order of age\n" +
                               "3 - to print pets in order of date registered\n" +
                               "4 - to print mammals only\n" +
                               "5 - to print birds only\n" +
                               "6 - to print fish only");
                
                System.out.println("\nEnter choice:");
                int choice = keyboard.nextInt();
                keyboard.nextLine();
            
                if(choice < 0 || choice > 6)
                {
                    System.out.println("Please enter one of the listed numbers (0-6)");
                    keyboard.nextLine();
                    continue;
                }    
                    switch(choice) 
                    {
                        case 0: 
                            printPetByID();
                            quit = true;
                            break;
                        case 1: 
                            printPetByGender(); 
                            quit = true;
                            break;
                        case 2:
                            printPetByAge();
                            quit = true;
                            break;
                        case 3:
                            printPetByDate();
                            quit = true;
                            break;
                        case 4:
                            printMammals();
                            quit = true;
                            break;
                        case 5:
                            printBirds();
                            quit = true;
                            break;
                        case 6:
                            printFish();
                            quit = true;
                            break;                        
                    }
                }
                catch(InputMismatchException e)
                {
                    System.out.println("Please enter one of the listed numbers (0-6)");
                    keyboard.nextLine();                            
                }
            }       
    }
    
    /**
     *Sorts pets by ID and prints the details
     */
    private static void printPetByID()
    {
        Collections.sort(pets, new PetComparatorByID());
        System.out.println(pets.toString());     
    }
    
    /**
     *Sorts pets by gender and prints the details
     */
    private static void printPetByGender()
    {
        Collections.sort(pets, new PetComparatorByGender());
        System.out.println(pets.toString());     
    }
    
    /**
     *Sorts pets by age and prints the details
     */
    private static void printPetByAge()
    {
        Collections.sort(pets, new PetComparatorByAge());
        System.out.println(pets.toString());     
    }
    
    /**
     *Sorts pets by registry date and prints the details
     */
    private static void printPetByDate()
    {
        Collections.sort(pets, new PetComparatorByDate());
        System.out.println(pets.toString());     
    }
    
    /**
     *Prints the registered mammals
     */
    private static void printMammals()
    {
        for (Mammal mammal : mammals) 
        {
            System.out.println(mammal.toString());
        }       
    }
    
    /**
     *Prints the registered birds
     */
    private static void printBirds()
    {
        for (Bird bird : birds) 
        {
            System.out.println(bird.toString());
        }      
    }
    
    /**
     *Prints the registered fish
     */
    private static void printFish()
    {
        for (Fish fish : fishes) 
        {
            System.out.println(fish.toString());
        }       
    }
    
    /**
     *Generates statistics for the registered pets
     */
    private static void generateStatistics()
    {
        if(pets.size() > 0)
        {
            System.out.println("\nPet Statistics:");
            System.out.println("\n");
            System.out.println("There are " + pets.size() + " registered pets");
        
            double numMammals = mammals.size();
            double numBirds = birds.size();
            double numFish = fishes.size();
            double numPets = pets.size();
            
            double percentageMammals = (numMammals/numPets) * 100;           
            double percentageBirds = (numBirds/numPets) * 100;
            double percentageFish = (numFish/numPets) * 100;
        
            System.out.println(percentageMammals + "% of the registered pets are mammals");
            System.out.println(percentageBirds + "% of the registered pets are birds");
            System.out.println(percentageFish + "% of the registered pets are fish");
        
            double numAssigned = countAssigned();
            double percentageRegistered = numAssigned/numPets * 100;
            System.out.println(percentageRegistered + "% of the registered pets are assigned to an owner");
        
            if(mammals.size() > 0)
            {
                double numNeutered = countNeutered();
                double percentageNeutered = numNeutered/numMammals * 100;
                System.out.println(percentageNeutered + "% of the registered mammals are neutered");
            }
        
            if(birds.size() > 0)
            {
                double numCanFly = countCanFly();
                double percentageCanFly = numCanFly/numBirds * 100;
                System.out.println(percentageCanFly + "% of the registered birds can fly");
            }
        }
        else
        {
            System.out.println("There are no registered pets to generate statistics for");
        }
    }
    
    /**
     *Counts the number of neutered mammals
     * @return count
     */
    private static double countNeutered()
    {
        double count = 0;
        for (Mammal mammal : mammals) 
        {
            if(mammal.isNeutered() == true)
            {
                count++;
            }
        }   
        return count;
    }
    
    /**
     *Counts the number of birds that can fly
     * @return count
     */
    private static double countCanFly()
    {
        double count = 0;
        for (Bird bird : birds) 
        {
            if(bird.isFly() == true)
            {
                count++;
            }
        }   
        return count;
    }
    
    /**
     *Counts the number of assigned pets
     * @return count
     */
    private static double countAssigned()
    {
        int count = 0;
        for (Pet pet : pets) 
        {
            if(pet.getOwnerID() != null)
            {
                count++;
            }
        }   
        return count;
    }
    
    /**
     *Prints the available choices for the main menu
     */
    private static void printChoices()
    {
        System.out.println("\nAvailable choices: \npress");
        System.out.println("0 - to shutdown\n" +
                           "1 - to add a new pet to the system\n" +
                           "2 - to edit/delete/print pet details\n" +
                           "3 - to add a new owner to the system\n" +
                           "4 - to edit/delete/print owner details\n" +
                           "5 - to assign a pet to an owner\n" +
                           "6 - to print all pets in a particular order\n" +
                           "7 - to generate pet statistics\n" +
                           "8 - to print list of available choieces");
    }   
}



        
    
    

