/**
 * Student ID: D00218208
 * Name: Patrick Nugent
 */
package PatrickNugentGD2OOPCA2;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Owners 
{
    /**
     *Writes owner data to a text file
     */
    public static void writeOwnerDataToTextFile()
    {
        try(FileWriter ownersFile = new FileWriter("owners.txt"))
        {
            for(Owner owner : Main.owners)
            {
                ownersFile.write(owner.getName() + "," + owner.getID() + "," + 
                owner.getEmail() + "," + owner.getTelephone() + "\n");
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
    
    /**
     *Reads owner data from a text file
     */
    public static void readOwnerDataFromTextFile()
    {
        try(Scanner scanner = new Scanner(new FileReader("pets.txt")))
        {
            scanner.useDelimiter(",");
            while (scanner.hasNextLine()) {
                String name = scanner.nextLine();
                scanner.skip(scanner.delimiter());
                String ID = scanner.nextLine();
                scanner.skip(scanner.delimiter());
                String email = scanner.nextLine();
                scanner.skip(scanner.delimiter());
                String telephone = scanner.nextLine();
                scanner.skip(scanner.delimiter());
                String address = scanner.nextLine();
                Owner importedOwner = new Owner(name, ID, email, telephone, address);
                Main.owners.add(importedOwner);
            }
        }
        catch(IOException e)
        {
            System.out.println("Failed to import owners");
        }
        catch(NoSuchElementException e)
        {
            System.out.println("Failed to import owners");
        }
    }
    
}
