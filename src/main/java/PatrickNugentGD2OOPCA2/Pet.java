/**
 * Student ID: D00218208
 * Name: Patrick Nugent
 */

package PatrickNugentGD2OOPCA2;

import java.io.Serializable;
import java.util.ArrayList;

public class Pet implements Serializable
{
    private static final long serialVersionUID = 1L;
    
    private String type;
    private String name;
    private String breed;
    private int age;
    private ArrayList<String> colour;
    private String gender;
    private String dateRegistered;
    private String petID;
    private String ownerID;  

    public Pet(String type, String name, String breed, int age, ArrayList<String> colour, String gender, String dateRegistered, String petID, String ownerID) 
    {
        this.type = type;
        this.name = name;
        this.breed = breed;
        this.age = age;
        this.colour = colour;
        this.gender = gender;
        this.dateRegistered = dateRegistered;
        this.petID = petID;
        this.ownerID = ownerID;
    }

    public String getType() 
    {
        return type;
    }

    public String getName() 
    {
        return name;
    }

    public String getBreed() 
    {
        return breed;
    }

    public int getAge() 
    {
        return age;
    }

    public ArrayList<String> getColour() 
    {
        return colour;
    }

    public String getGender() 
    {
        return gender;
    }

    public String getDateRegistered() 
    {
        return dateRegistered;
    }

    public String getPetID() 
    {
        return petID;
    }

    public String getOwnerID() 
    {
        return ownerID;
    }

    public void setType(String type) 
    {
        this.type = type;
    }

    public void setName(String name) 
    {
        this.name = name;
    }

    public void setBreed(String breed) 
    {
        this.breed = breed;
    }

    public void setAge(int age) 
    {
        this.age = age;
    }

    public void setColour(ArrayList<String> colour) 
    {
        this.colour = colour;
    }

    public void setGender(String gender) 
    {
        this.gender = gender;
    }

    public void setDateRegistered(String dateRegistered) 
    {
        this.dateRegistered = dateRegistered;
    }

    public void setPetID(String petID) 
    {
        this.petID = petID;
    }

    public void setOwnerID(String ownerID) 
    {
        this.ownerID = ownerID;
    }   
    
    @Override
    public String toString() {
        return "\nType:\t" + type + "\nName:\t" + name + "\nBreed:\t" + breed + "\nAge:\t" + age + "\nColour:\t" + colour + "\nGender:\t" + gender + "\nDate Registered:   " + dateRegistered + "\nPet ID:\t" + petID + "\nOwner ID:   " + ownerID;
    }
    
}
