/**
 * Student ID: D00218208
 * Name: Patrick Nugent
 */
package PatrickNugentGD2OOPCA2;

import java.util.Comparator;

public class PetComparatorByGender implements Comparator<Pet> 
{
    @Override
    public int compare(Pet petOne, Pet petTwo)
    {
        return petOne.getGender().compareTo(petTwo.getGender());
    }   
}
