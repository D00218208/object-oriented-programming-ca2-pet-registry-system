/**
 * Student ID: D00218208
 * Name: Patrick Nugent
 */

package PatrickNugentGD2OOPCA2;

import java.io.Serializable;

public class Owner implements Serializable
{
    private String name;
    private String ID;
    private String email;
    private String telephone;
    private String address;
    
    public Owner(String name, String ID, String email, String Telephone, String address) 
    {
        this.name = name;
        this.ID = ID;
        this.email = email;
        this.telephone = Telephone;
        this.address = address;
    }

    public String getName() 
    {
        return name;
    }

    public String getID() 
    {
        return ID;
    }

    public String getEmail() 
    {
        return email;
    }

    public String getTelephone() 
    {
        return telephone;
    }

    public String getAddress() 
    {
        return address;
    }

    public void setName(String name) 
    {
        this.name = name;
    }

    public void setEmail(String email) 
    {
        this.email = email;
    }

    public void setTelephone(String telephone) 
    {
        this.telephone = telephone;
    }

    public void setAddress(String address) 
    {
        this.address = address;
    }
    
    
    
    @Override
    public String toString() {
        return "\nName:\t" + name + "\nID:\t" + ID + "\nEmail:\t" + email + "\nTelephone:\t" + telephone + "\nHome address:\t" + address;
    }
    
    
}


