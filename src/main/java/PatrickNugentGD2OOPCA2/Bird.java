/**
 * Student ID: D00218208
 * Name: Patrick Nugent
 */

package PatrickNugentGD2OOPCA2;

import java.util.ArrayList;

public class Bird extends Pet
{
    private double wingspan;
    private boolean fly;

    public Bird(String type, String name, String breed, int age, ArrayList<String> colour, String gender, double wingspan, boolean fly, String dateRegistered, String petID, String ownerID) {
        super(type, name, breed, age, colour, gender, dateRegistered, petID, ownerID);
        this.wingspan = wingspan;
        this.fly = fly;
    }

    public double getWingspan() 
    {
        return wingspan;
    }

    public boolean isFly() 
    {
        return fly;
    }

    public void setWingspan(double wingspan) 
    {
        this.wingspan = wingspan;
    }

    public void setFly(boolean fly) 
    {
        this.fly = fly;
    }  

    @Override
    public int hashCode() 
    {
        int hash = 7;
        hash = 31 * hash + (int) (Double.doubleToLongBits(this.wingspan) ^ (Double.doubleToLongBits(this.wingspan) >>> 32));
        hash = 31 * hash + (this.fly ? 1 : 0);
        return hash;
    }  

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Bird other = (Bird) obj;
        if (Double.doubleToLongBits(this.wingspan) != Double.doubleToLongBits(other.wingspan)) {
            return false;
        }
        if (this.fly != other.fly) {
            return false;
        }
        if(this.getType().equals(other.getType()) && this.getName().equals(other.getName())
        && this.getBreed().equals(other.getBreed()) && this.getAge() == other.getAge()
        && this.getColour().equals(other.getColour()) && this.getDateRegistered().equals(other.getDateRegistered())
        && this.getGender().equals(other.getGender())) {
            return true;
        }
        return false;
    }
        
    @Override
    public String toString() {
        return super.toString() + "\nWingspan (inches):   " + wingspan + "\nCan fly:   " + fly + "\n";
    }
    
}
