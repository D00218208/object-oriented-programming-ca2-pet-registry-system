/**
 * Student ID: D00218208
 * Name: Patrick Nugent
 */

package PatrickNugentGD2OOPCA2;

import java.util.ArrayList;

public class Mammal extends Pet 
{
    private boolean neutered;

    public Mammal(String type, String name, String breed, int age, ArrayList<String> colour, String gender, boolean neutered, String dateRegistered, String petID, String ownerID) {
        super(type, name, breed, age, colour, gender, dateRegistered, petID, ownerID);
        this.neutered = neutered;
    }

    public boolean isNeutered() 
    {
        return neutered;
    }

    public void setNeutered(boolean neutered) 
    {       
        this.neutered = neutered;
    }

    @Override
    public int hashCode() 
    {
        int hash = 3;
        hash = 79 * hash + (this.neutered ? 1 : 0);
        return hash;
    }  

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Mammal other = (Mammal) obj;
        if (this.neutered != other.neutered) {
            return false;
        }
        if(this.getType().equals(other.getType()) && this.getName().equals(other.getName())
        && this.getBreed().equals(other.getBreed()) && this.getAge() == other.getAge()
        && this.getColour().equals(other.getColour()) && this.getDateRegistered().equals(other.getDateRegistered())
        && this.getGender().equals(other.getGender())) {
            return true;
        }
        return false;
    }
                  
    @Override
    public String toString() {
        return super.toString() + "\nNeutered:   " + neutered + "\n";
    }
}
