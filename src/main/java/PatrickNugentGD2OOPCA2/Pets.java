/**
 * Student ID: D00218208
 * Name: Patrick Nugent
 */
package PatrickNugentGD2OOPCA2;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Pets
{   
    /**
     *Writes pet data to a text file
     */
    public static void writePetDataToTextFile()
    {
        try(FileWriter petsFile = new FileWriter("pets.txt"))
        {
            for(Pet pet : Main.pets)
            {
                petsFile.write(pet.getType() + "," + pet.getName() + "," + 
                pet.getBreed() + "," + pet.getAge() + "," + pet.getGender() + 
                "," + pet.getDateRegistered() + "," + pet.getPetID() + "," + 
                pet.getOwnerID() + "\n");
            }
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
    }
    
    /**
     *Reads pet data from a text file
     */
    public static void readPetDataFromTextFile()
    {
        try(Scanner scanner = new Scanner(new FileReader("pets.txt")))
        {
            scanner.useDelimiter(",");
            while (scanner.hasNextLine()) {
                String type = scanner.nextLine();
                scanner.skip(scanner.delimiter());
                String name = scanner.nextLine();
                scanner.skip(scanner.delimiter());
                String breed = scanner.nextLine();
                scanner.skip(scanner.delimiter());
                int age = scanner.nextInt();
                scanner.skip(scanner.delimiter());
                String gender = scanner.nextLine();
                scanner.skip(scanner.delimiter());
                String dateRegistered = scanner.nextLine();
                scanner.skip(scanner.delimiter());
                String petID = scanner.nextLine();
                scanner.skip(scanner.delimiter());
                String ownerID = scanner.nextLine();
                Pet importedPet = new Pet(type, name, breed, age, null, gender, dateRegistered, petID, ownerID);
                Main.pets.add(importedPet);
            }
        }
        catch(IOException e)
        {
            System.out.println("Failed to import pets");
        }
        catch(NoSuchElementException e)
        {
            System.out.println("Failed to import pets");
        }
    }
    
}
