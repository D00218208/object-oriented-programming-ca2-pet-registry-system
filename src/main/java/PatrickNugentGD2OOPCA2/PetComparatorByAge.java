/**
 * Student ID: D00218208
 * Name: Patrick Nugent
 */
package PatrickNugentGD2OOPCA2;

import java.util.Comparator;

public class PetComparatorByAge implements Comparator<Pet> 
{
    @Override
    public int compare(Pet petOne, Pet petTwo)
    {
        int flag = petOne.getAge() - petTwo.getAge();
        return flag;
    }   
}
