/**
 * Student ID: D00218208
 * Name: Patrick Nugent
 */

package PatrickNugentGD2OOPCA2;

import java.util.ArrayList;
import java.util.Objects;

public class Fish extends Pet
{
    private String waterType;

    public Fish(String type, String name, String breed, int age, ArrayList<String> colour, String gender, String waterType, String dateRegistered, String petID, String ownerID) {
        super(type, name, breed, age, colour, gender, dateRegistered, petID, ownerID);
        this.waterType = waterType;
    }

    public String getWaterType()
    {
        return waterType;
    }

    public void setWaterType(String waterType) 
    {
        this.waterType = waterType;
    }

    @Override
    public int hashCode() 
    {
        int hash = 5;
        hash = 47 * hash + Objects.hashCode(this.waterType);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Fish other = (Fish) obj;
        if (!Objects.equals(this.waterType, other.waterType)) {
            return false;
        }
        if(this.getType().equals(other.getType()) && this.getName().equals(other.getName())
        && this.getBreed().equals(other.getBreed()) && this.getAge() == other.getAge()
        && this.getColour().equals(other.getColour()) && this.getDateRegistered().equals(other.getDateRegistered())
        && this.getGender().equals(other.getGender())) {
            return true;
        }
        return false;
    }
     
    @Override
    public String toString() {
        return super.toString() + "\nRequired water type:   " + waterType + "\n";
    }
    
}
